<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//custom routes
$router->post('api/login', 'UsersController@authenticate');
$router->group(['prefix' => 'api/', 'middleware' => ['auth']], function ($app) {
	//subscribers
	$app->get('/subscribers', 'SubscribersController@index');
	$app->post('/subscriber/store', 'SubscribersController@store');
	$app->get('/subscriber/show/{id}', 'SubscribersController@show');
	$app->post('/subscriber/update/{id}', 'SubscribersController@update');
	$app->post('/subscriber/delete/{id}', 'SubscribersController@destroy');
	
	//lists
	$app->get('/lists', 'ListingsController@index');
	$app->post('/list/store', 'ListingsController@store');
	$app->get('/list/show/{id}', 'ListingsController@show');
	$app->post('/list/update/{id}', 'ListingsController@update');
	$app->delete('/list/delete/{id}', 'ListingsController@destroy');
});