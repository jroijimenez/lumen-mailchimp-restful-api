<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Http\Controllers\SubscribersController;
use App\User;

class SubscribersTest extends TestCase
{
    public $apiKey = null;
    
    public function setup()
    {
        parent::setUp();
        
        $user = User::find(1);
        $this->apiKey = $user->api_key;
    }
    
    public function testWithoutToken()
    {
        $response = $this->call('GET', '/api/subscribers');
        $this->assertEquals(401, $response->status());
    }
    
    public function testIndex()
    {
        $response = $this->call('GET', '/api/subscribers', [], [], [], [
            'HTTP_api_key' => $this->apiKey
        ]);
        $this->assertJson($response->content());
    }
    
    public function testShow()
    {
        $response = $this->call('GET', '/api/subscriber/show/2', [], [], [], [
            'HTTP_api_key' => $this->apiKey
        ]);
        $this->assertJson($response->content());
    }
    
    public function testStoreWithoutEmail()
    {
        $response = $this->call('POST', '/api/subscriber/store', [
            'email' => ''
        ], [], [], [
            'HTTP_api_key' => $this->apiKey
        ]);
        $this->assertEquals(422, $response->status());
    }
    
    public function testStore()
    {
        $response = $this->call('POST', '/api/subscriber/store', [
            'email' => 'jroijimenez@gmail.com',
            'first_name' => 'JRoi',
            'last_name' => 'Test',
            'list_id' => '562f902855'
        ], [], [], [
            'HTTP_api_key' => $this->apiKey
        ]);
        $this->assertJson($response->content());
    }
    
    public function testUpdate()
    {
        $response = $this->call('POST', '/api/subscriber/update/2', [
            'email' => 'jroi.jimenez@gmail.com',
            'first_name' => 'JRoi',
            'last_name' => 'Updated',
            'list_id' => '562f902855'
        ], [], [], [
            'HTTP_api_key' => $this->apiKey
        ]);
        $this->assertJson($response->content());
    }
    
    public function testDetroy()
    {
        $response = $this->call('POST', '/api/subscriber/delete/2', [], [], [], [
            'HTTP_api_key' => $this->apiKey,
            'list_id' => '562f902855'
        ]);
        $this->assertJson($response->content());
    }
}
