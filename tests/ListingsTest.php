<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Http\Controllers\ListingsController;
use App\User;

class ListingsTest extends TestCase
{
    public $apiKey = null;
    
    public function setup()
    {
        parent::setUp();
        
        $user = User::find(1);
        $this->apiKey = $user->api_key;
    }
    
    public function testWithoutToken()
    {
        $response = $this->call('GET', '/api/lists');
        $this->assertEquals(401, $response->status());
    }
    
    public function testIndex()
    {
        $response = $this->call('GET', '/api/lists', [], [], [], [
            'HTTP_api_key' => $this->apiKey
        ]);
        $this->assertJson($response->content());
    }
    
    public function testShow()
    {
        $response = $this->call('GET', '/api/list/show/2', [], [], [], [
            'HTTP_api_key' => $this->apiKey
        ]);
        $this->assertJson($response->content());
    }
    
    public function testStoreWithoutName()
    {
        $response = $this->call('POST', '/api/list/store', [
            'name' => ''
        ], [], [], [
            'HTTP_api_key' => $this->apiKey
        ]);
        $this->assertEquals(422, $response->status());
    }
    
    public function testStore()
    {
        $response = $this->call('POST', '/api/list/store', [
            'name' => 'List Name'
        ], [], [], [
            'HTTP_api_key' => $this->apiKey
        ]);
        $this->assertJson($response->content());
    }
    
    public function testUpdate()
    {
        $response = $this->call('POST', '/api/list/update/2', [
            'name' => 'Unit Test'
        ], [], [], [
            'HTTP_api_key' => $this->apiKey
        ]);
        $this->assertJson($response->content());
    }
    
    public function testDetroy()
    {
        $response = $this->call('DELETE', '/api/list/delete/2', [], [], [], [
            'HTTP_api_key' => $this->apiKey
        ]);
        $this->assertJson($response->content());
    }
    
    /* public function testIndex()
    {
		//check if the variable is empty
        $stack = [];
        $this->assertEmpty($stack);
		return $stack;
		
		//$response = $this->call('GET', '/api/lists');
		//$response = $this->call('POST', '/api/login');
        
        //$listings = new ListingsController('c5f9cab7bb31c47944d373fca237de0f-us14');
        //$this->assertEquals('yes', $listings->index());
        
        $response = $this->call('GET', '/api/lists');
        $this->assertEquals(401, $response->status());
        
		echo "<pre>";
		print_r($response->content());
		echo "</pre>";
		die();
    } */
}
