<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //insert test data
        DB::table('users')->insert([
            'email' => 'jroi.jimenez@gmail.com',
            'password' => Hash::make('test123'),
            'first_name' => 'Jeffrey Roi',
            'last_name' => 'Jimenez',
            'api_key' => base64_encode(str_random(64)),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
