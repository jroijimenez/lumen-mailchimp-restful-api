<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ConfigurationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //insert test data
        DB::table('configurations')->insert([
            'name' => 'mailchimp_key',
            'value' => '5ef1e8024453cfcca8f49ba509089be1-us17',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
