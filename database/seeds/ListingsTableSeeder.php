<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ListingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //insert test data
        DB::table('listings')->insert([
            'name' => 'Flexisource List',
            'list_id' => '562f902855',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
