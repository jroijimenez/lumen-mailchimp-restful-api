<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash; //for handling passwords

class UsersController extends Controller
{
    //authenticate request to get the API key
    public function authenticate(Request $request)
    {
        //validate request data
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);
        
        //authenticate the request and return an API key
        $email = $request->input('email');
        $user = User::where('email', $email)->first();
        //if user exists, check if the password matches and then update the API key and return it 
        if ($user) {
            if (Hash::check($request->input('password'), $user->password)) {
                $user->api_key = base64_encode(str_random(64));
                $user->save();
                return response()->json(['error'=>false, 'api_key'=> $user->api_key]); 
            }
            return response()->json(['error'=>true, 'message'=>'Password is incorrect.']); 
        }
        return response()->json(['error'=>true, 'message'=>'User does not exists.']); 
    }
}
