<?php

namespace App\Http\Controllers;

use App\Listing;
use App\Configuration;
use Illuminate\Http\Request;

class ListingsController extends Controller
{
    //Note: change the API key and list ID from your mailchimp account
    protected $mailChimp = '';
    
    //get the API key from the configurations table
    public function __construct($mailchimpKey = false)
    {
        if (!$mailchimpKey) {
            $getDbKey = Configuration::where('name', 'mailchimp_key')->first();
            $apiKey = $getDbKey->value;
        } else {
            $apiKey = $mailchimpKey;
        }
		$this->mailChimp = new \App\Libraries\MailChimp($apiKey);
    }
    
    //show all lists
    public function index(Request $request)
    {
        $list = Listing::get();
        return response()->json(['error'=>false, 'list'=> $list]); 
    }
    
    //get a specific list
    public function show(Request $request, $id)
    {
        //get a individual list
        $list = Listing::where('id', (int)$id)->first();
        
        //validate request data
        if (!$list) {
            return response()->json(['error'=>true, 'message'=>'List does not exists.']); 
        }
        
        return response()->json(['error'=>false, 'list'=> $list]);
    }
    
    //add new list
    public function store(Request $request)
    {
        //validate request data
        $this->validate($request, [
            'name' => 'required|unique:listings'
        ]);
        
        //insert the list to the database
        $name = $request->input('name');
        
        $listing = new Listing();
        $listing->name = $name;
        $listing->save();
        
        //insert to mailchimp
        $mailchimpResultArr = $this->mailChimp->post("lists", [
            'name' => $name,
            'contact' => array(
                            'company' => 'Test Company',
                            'address1' => 'Address 1',
                            'city' => 'Makati City',
                            'state' => 'State Name',
                            'zip' => '1234',
                            'country' => 'PH',
                        ),
            'permission_reminder' => 'Sample reminder why they are subscribed.',
            'campaign_defaults' => array(
                            'from_name' => 'Test Name',
                            'from_email' => 'sample@gmail.com',
                            'subject' => 'Email Subject',
                            'language' => 'English'
                        ),
            'email_type_option' => false
        ]);

        //update list's table for mailchimp ID
        $listing->list_id = $mailchimpResultArr['id'];
        $listing->save();
        return response()->json(['error'=>false, 'message'=> 'List successfully added.']); 
    }

    //update list
    public function update(Request $request, $id)
    {
        //get a individual list
        $list = Listing::where('id', (int)$id)->first();
        
        //validate request data
        if (!$list) {
            return response()->json(['error'=>true, 'message'=>'List does not exists.']); 
        }
        
        //validate request data
        $this->validate($request, [
            'name' => 'required|unique:listings,name,'.$id
        ]);

        //update list in the database
        $name = $request->input('name');
        $list->name = $name;
        $list->save();
        
        //update in mailchimp
        $listId = $list->list_id;
        $mailchimpResultArr = $this->mailChimp->patch("lists/$listId", [
            'name' => $name,
        ]);
        return response()->json(['error'=>false, 'message'=> 'List successfully updated.']); 
    }

    //delete list
    public function destroy(Request $request, $id)
    {
        //get a individual list
        $list = Listing::where('id', (int)$id)->first();
        
        //validate request data
        if (!$list) {
            return response()->json(['error'=>true, 'message'=>'List does not exists.']); 
        }

        //delete list in the database
        $listId = $list->list_id;
        $list->delete();
        
        //remove in mailchimp
        $mailchimpResultArr = $this->mailChimp->delete("lists/$listId");
        return response()->json(['error'=>false, 'message'=> 'List successfully removed.']); 
    }
}
