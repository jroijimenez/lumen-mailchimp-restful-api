<?php

namespace App\Http\Controllers;
use App\Subscriber;
use App\Configuration;
use Illuminate\Http\Request;

class SubscribersController extends Controller
{
    //Note: change the API key and list ID from your mailchimp account
    protected $mailChimp = '';
    
    //get the API key from the configurations table
    public function __construct($mailchimpKey = false)
    {
        if (!$mailchimpKey) {
            $getDbKey = Configuration::where('name', 'mailchimp_key')->first();
            $apiKey = $getDbKey->value;
        } else {
            $apiKey = $mailchimpKey;
        }
		$this->mailChimp = new \App\Libraries\MailChimp($apiKey);
    }
    
    //show all subscribers
    public function index(Request $request)
    {
        $subscribers = Subscriber::get();
        return response()->json(['error'=>false, 'subscribers'=> $subscribers]); 
    }
    
    //get a specific subscriber
    public function show(Request $request, $id)
    {
        //get a individual subscriber
        $subscriber = Subscriber::where('id', (int)$id)->first();
        
        //validate request data
        if (!$subscriber) {
            return response()->json(['error'=>true, 'message'=>'Subscriber does not exists.']); 
        }

        return response()->json(['error'=>false, 'subscriber'=> $subscriber]); 
    }
    
    //add new subscriber
    public function store(Request $request)
    {
        //validate request data
        $this->validate($request, [
            'email' => 'required|email|unique:subscribers',
            'list_id' => 'required|exists:listings'
        ]);
        
        //insert the subscriber to the database
        $email = $request->input('email');
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $this->listId = $request->input('list_id');
        $subscriber = new Subscriber();
        $subscriber->email = $email;
        $subscriber->first_name = $first_name;
        $subscriber->last_name = $last_name;
        $subscriber->save();
        
        //insert to mailchimp
        $mailchimpResultArr = $this->mailChimp->post("lists/$this->listId/members", [
            'email_address' => $email,
            'merge_fields' => ['FNAME'=>$first_name, 'LNAME'=>$last_name],
            'status'        => 'subscribed',
        ]);
        
        //update subscriber's table for mailchimp ID
        if (isset($mailchimpResultArr['id'])) {
            $subscriber->mailchimp_id = $mailchimpResultArr['id'];
            $subscriber->save();
        }
        return response()->json(['error'=>false, 'message'=> 'Subscriber successfully added.']); 
    }

    //update subscriber
    public function update(Request $request, $id)
    {
        //validate request data
        $this->validate($request, [
            'email' => 'required|email',
            'list_id' => 'required|exists:listings'
        ]);
        
         //get a individual subscriber
        $subscriber = Subscriber::where('id', (int)$id)->first();
        
        //validate request data
        if (!$subscriber) {
            return response()->json(['error'=>true, 'message'=>'Subscriber does not exists.']); 
        }

        //update subscriber in the database
        $email = $request->input('email');
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $this->listId = $request->input('list_id');
        $subscriber->email = $email;
        $subscriber->first_name = $first_name;
        $subscriber->last_name = $last_name;
        $subscriber->save();
        
        //update in mailchimp
        $mailchimpId = $subscriber->mailchimp_id;
        $mailchimpResultArr = $this->mailChimp->patch("lists/$this->listId/members/$mailchimpId", [
            'email_address' => $email,
            'merge_fields' => ['FNAME'=>$first_name, 'LNAME'=>$last_name]
        ]);
        //update subscriber's table for mailchimp ID
        if (isset($mailchimpResultArr['id'])) {
            $subscriber->mailchimp_id = $mailchimpResultArr['id'];
            $subscriber->save();
        }
        return response()->json(['error'=>false, 'message'=> 'Subscriber successfully updated.']); 
    }

    //delete subscriber
    public function destroy(Request $request, $id)
    {
        //validate request data
        $this->validate($request, [
            'list_id' => 'required|exists:listings'
        ]);
        
        //get a individual subscriber
        $subscriber = Subscriber::where('id', (int)$id)->first();
        
        //validate request data
        if (!$subscriber) {
            return response()->json(['error'=>true, 'message'=>'Subscriber does not exists.']); 
        }
        
        $this->listId = $request->input('list_id');

        //delete subscriber in the database
        $mailchimpId = $subscriber->mailchimp_id;
        $subscriber->delete();
        
        //remove in mailchimp
        $mailchimpResultArr = $this->mailChimp->delete("lists/$this->listId/members/$mailchimpId");
        return response()->json(['error'=>false, 'message'=> 'Subscriber successfully removed.']); 
    }
}
